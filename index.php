<?php

ob_start();

function immediateOutput($str) {
    echo str_repeat(" ", 4096) . nl2br($str); // Required for some browsers (output is not displayed if buffer size is less than 1KB
    ob_end_flush();
    ob_flush();
    flush();
    ob_start();
}

immediateOutput(date("Y-m-d H:i:s") . PHP_EOL);
sleep(3);
immediateOutput(date("Y-m-d H:i:s") . PHP_EOL);
sleep(3);
echo date("Y-m-d H:i:s") . PHP_EOL;
